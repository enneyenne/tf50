variable "yc_cloud" {
  type        = string
  description = "YC ID"
}

variable "yc_folder" {
  type        = string
  description = "YC FOLDER ID"
}

variable "yc_token" {
  type        = string
  description = "YC TOKEN"
}

variable "yc_zone" {
  type        = string
  description = "YC TOKEN"
}